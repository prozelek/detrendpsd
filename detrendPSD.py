import numpy as np
import scipy.stats.mstats as ms

def detrendPSD(data, n, overlap):
    """
    Calculate PSD for detrended signal.

    Parameters
        ---------
        data : array_like
            One-dimentional array containing the signal to be analyzed.
        n : int
            Window length.
        overlap : float in range [0,1)
            Window's overlap.
        
    Returns
        -------
        psdAriphmetic : ndarray
            Array of PSD values. PSD calculated by all windows's PSD ariphmetic averaging.
        psdGeometric : ndarray
            Array of PSD values. PSD calculated by all windows's PSD geometric averaging.
        coeffsAll: ndarray
            Array of detrend polynoms coefficients. Array are contatenated coefficients (highest power first)
            in least squares quadratic polynom fit for each windiw.
    """
    if not isinstance(data, (list, tuple, np.ndarray)):
        raise TypeError("Input data not a array-like")
        
    data = np.asarray(data)
    
    if not len(data.shape)==1:
        raise ValueError("Input data must be one-dimentional array")
    
    if data.size < 2:
        raise ValueError("Input data has incorrect length. It must be more then 1")
    
    if np.sum(np.isreal(data))<data.size:
        raise ValueError("Input data must be array of real")
            
    if not isinstance(n,int):
        if np.isreal(n):
            if not n.is_integer():
                 raise ValueError("Parameter n must be an integer")
        else:
            raise ValueError("Parameter n must be an integer")
    n=int(n)
    
    if n>data.size or n<2:
        raise ValueError("Parameter n must be <= length of data and more then 1")
    
    if not np.isreal(overlap):
        raise ValueError("Parameter overlap must be float")
    overlap = float(overlap)       
           
    if overlap<0 or overlap>=1:
        raise ValueError("Parameter overlap must be in range from 0 (incl.) to 1 (no incl.)")
        
    #number of slices by solving equation (i+1)*n-i*n*overlap<len
    numParts=int((data.shape[0]-n)//(n-n*overlap))+1

    #init
    psdAll=np.zeros([numParts,int(n/2)+1])
    coeffsAll=np.zeros(3*numParts)
    
    for i in xrange(0,numParts):
        #get window
        slice1=data[int(i*n-i*n*overlap):int((i+1)*n-i*n*overlap)]
        
        #get coeffs of least squares quadratic polynom fit
        coeffs=np.polyfit(np.arange(0, n, 1), slice1, 2)
        #put coeffs to output
        coeffsAll[i*3:i*3+3]=coeffs
        poly = np.poly1d(coeffs)
        #calculate the quadratic polynom values
        y=map(poly,xrange(0,n))
        #detrending window
        sliceDetrend=slice1-y

        #calculate PSD. Suppose frequency is 1.0 Hz
        fs=1.0
        psd=(1.0/(fs*n))*(np.abs(np.fft.rfft(sliceDetrend))**2)

        #Because the signal is real-valued, we need power estimates for the positive or negative frequencies.
        #In order to conserve the total power, multiply all frequencies that occur
        #in both sets -- the positive and negative frequencies -- by a factor of 2. 
        #Zero frequency (DC) and the Nyquist frequency do not occur twice
        psd[1:-1]=2*psd[1:-1]
        psdAll[i,:]=psd        
        #transform PSD to Power/Frequency (dB/Hz)
        #psdAll[i,:]=10*np.log10(psdAll[i,:])
        #but in this case we have neg. values that can't provide geometric mean

    #calculate mean PSD
    psdArithmetic=np.mean(psdAll,0)
    if numParts<2:
        psdGeometric=ms.gmean(psdAll,0)
    else:
        psdGeometric=psdAll[0,:]
        
    return (psdArithmetic,psdGeometric,coeffsAll)